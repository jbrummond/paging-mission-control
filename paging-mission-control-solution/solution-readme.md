Paging Mission Control Solution - Jacob Brummond

Use Maven to build and execute the solution

In Git Bash, run the following steps:
1. Build the project and run the unit tests: $ mvn clean package
2. Run the Paging Mission Control application with the desired text file: $ mvn exec:java -Dexec.mainClass=com.jbrummond.paging.mission.control.PagingMissionControl -Dexec.args="src/test/resources/sample-telemetry-data.txt"