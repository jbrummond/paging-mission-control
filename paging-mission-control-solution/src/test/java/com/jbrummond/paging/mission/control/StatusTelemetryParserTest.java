package com.jbrummond.paging.mission.control;

import org.junit.Before;
import org.junit.Test;

import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Unit tests for PagingMissionControl
 *
 * @author jbrummond
 */
public class StatusTelemetryParserTest {
    private Path workingDir;

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMdd HH:mm:ss.SSS");

    @Before
    public void init() {
        this.workingDir = Path.of("", "src/test/resources");
    }

    /**
     * Test case for the provided Paging Mission Control expected input and output
     *
     */
    @Test
    public void testStatusTelemetryParser() {
        StatusTelemetryRecord expectedStatusTelemetryRecord = new StatusTelemetryRecord(
                LocalDateTime.parse("20180101 23:01:05.001", dateTimeFormatter),
                1001,
                101,
                20,
                99.9,
                "TSTAT"
        );

        Path sampleTelemetryDataFile = this.workingDir.resolve("sample-telemetry-data.txt");
        List<StatusTelemetryRecord> statusTelemetryRecordList = StatusTelemetryParser.parseTelemetryFile(sampleTelemetryDataFile.toString());

        // Verify the number of statue telemetry records is correct
        assertEquals(14, statusTelemetryRecordList.size());
        // Verify the first status telemetry record is correct
        assertStatusTelemetryRecord(expectedStatusTelemetryRecord, statusTelemetryRecordList.get(0));
    }

    private void assertStatusTelemetryRecord(StatusTelemetryRecord expectedRecord, StatusTelemetryRecord actualRecord) {
        assertEquals(expectedRecord.getTimestamp().toString(), actualRecord.getTimestamp().toString());
        assertEquals(expectedRecord.getSatelliteId(), actualRecord.getSatelliteId());
        assertEquals(expectedRecord.getRedHighLimit(), actualRecord.getRedHighLimit());
        assertEquals(expectedRecord.getRedLowLimit(), actualRecord.getRedLowLimit());
        assertEquals(expectedRecord.getRawValue(), actualRecord.getRawValue(), 0.0);
        assertEquals(expectedRecord.getComponent(), actualRecord.getComponent());
    }
}
