package com.jbrummond.paging.mission.control;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;

import org.junit.Before;
import org.junit.Test;

/**
 * Unit tests for PagingMissionControl
 *
 * @author jbrummond
 */
public class PagingMissionControlTest
{
    private Path workingDir;

    private final ByteArrayOutputStream consoleOutputStream = new ByteArrayOutputStream();

    @Before
    public void init() {
        this.workingDir = Path.of("", "src/test/resources");
        System.setOut(new PrintStream(consoleOutputStream));
    }

    /**
     * Test case for the provided Paging Mission Control expected input and output
     *
     * @throws IOException
     */
    @Test
    public void testPagingMissionControl() throws IOException {
        runPagingMissionControl("sample-telemetry-data.txt", "paging-mission-control-expected-output.json");
    }

    /**
     * Test case to verify multiple alerts of the same type can be sent and multiple alerts for the same satellite
     *
     * @throws IOException
     */
    @Test
    public void testPagingMissionControlMultipleAlerts() throws IOException {
        runPagingMissionControl("sample-telemetry-data-multiple-alerts.txt", "paging-mission-control-expected-output-multiple-alerts.json");
    }

    /**
     * Test that battery alert is only for the same satellite and time range is within 5 minutes for 3 entires
     *
     * @throws IOException
     */
    @Test
    public void testPagingMissionControlNoBatteryAlert() throws IOException {
        runPagingMissionControl("sample-telemetry-data-no-battery-alert.txt", "paging-mission-control-expected-output-no-battery-alert.json");
    }

    /**
     * Test that thermostat alert is only for the same satellite and time range is within 5 minutes for 3 entries
     *
     * @throws IOException
     */
    @Test
    public void testPagingMissionControlNoThermostatAlert() throws IOException {
        runPagingMissionControl("sample-telemetry-data-no-therm-alert.txt", "paging-mission-control-expected-output-no-therm-alert.json");
    }

    /**
     * Test code can handle output when no alerts are sent
     *
     * @throws IOException
     */
    @Test
    public void testPagingMissionControlNoAlerts() throws IOException {
        runPagingMissionControl("sample-telemetry-data-no-alerts.txt", "paging-mission-control-expected-output-no-alerts.json");
    }

    private void runPagingMissionControl(String filePath, String expectedResultFilePath) throws IOException {
        Path sampleTelemetryDataFile = this.workingDir.resolve(filePath);
        PagingMissionControl.main(new String[]{sampleTelemetryDataFile.toString()});
        Path pagingMissionControlExpectedOutput = this.workingDir.resolve(expectedResultFilePath);
        String pagingMissionControlExpectedOutputString =
                new String(Files.readAllBytes(pagingMissionControlExpectedOutput));
        assertEquals(pagingMissionControlExpectedOutputString, consoleOutputStream.toString());
    }
}
