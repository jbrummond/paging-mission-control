package com.jbrummond.paging.mission.control;

import java.util.List;

/**
 * Paging Mission Control Application
 *
 * @author jbrummond
 */
public class PagingMissionControl
{
    public static void main( String[] args )
    {
        List<StatusTelemetryRecord> statusTelemetryRecordList = StatusTelemetryParser.parseTelemetryFile(args[0]);
        StatusTelemetryProcessor.processStatusTelemetry(statusTelemetryRecordList);
    }
}
