package com.jbrummond.paging.mission.control;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Stream;

/**
 * Parses the state telemetry data
 *
 * @author jbrummond
 */
public class StatusTelemetryParser {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMdd HH:mm:ss.SSS");

    /**
     * Parses the state telemetry data into the StatusTelemetryRecord POJO
     *
     * @param telemetryFile Input status telemetry file
     * @return List of status telemetry records
     */
    protected static List<StatusTelemetryRecord> parseTelemetryFile(String telemetryFile) {
        List<StatusTelemetryRecord> statusTelemetryRecordList = null;
        try (Stream<String> telemetryFileStream = Files.lines(Paths.get(telemetryFile))) {
            statusTelemetryRecordList = telemetryFileStream.map(StatusTelemetryParser::parseTelemetryFileLine).toList();
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        return statusTelemetryRecordList;
    }

    /**
     * Parses each status telemetry file line
     *
     * @param telemetryFileLine Current telemetry file line to parse
     * @return POJO containing the state telemetry record
     */
    private static StatusTelemetryRecord parseTelemetryFileLine(String telemetryFileLine) {
        String[] list = telemetryFileLine.split("\\|");

        StatusTelemetryRecord statusTelemetryRecord = new StatusTelemetryRecord();
        statusTelemetryRecord.setTimestamp(LocalDateTime.parse(list[0], dateTimeFormatter));
        statusTelemetryRecord.setSatelliteId(Integer.parseInt(list[1]));
        statusTelemetryRecord.setRedHighLimit(Integer.parseInt(list[2]));
        statusTelemetryRecord.setRedLowLimit(Integer.parseInt(list[5]));
        statusTelemetryRecord.setRawValue(Double.parseDouble(list[6]));
        statusTelemetryRecord.setComponent(list[7]);

        return statusTelemetryRecord;
    }
}
