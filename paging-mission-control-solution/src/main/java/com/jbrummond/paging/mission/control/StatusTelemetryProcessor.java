package com.jbrummond.paging.mission.control;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.util.DefaultIndenter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Processor class for creating status telemetry alert messages
 *
 * @author jbrummond
 */
public class StatusTelemetryProcessor {

    private static final DateTimeFormatter resultDateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

    /**
     * Method to process state telemetry records and create/send state telemetry alert message(s)
     *
     * @param statusTelemetryRecordList List of parsed status telemetry records
     */
    protected static void processStatusTelemetry(List<StatusTelemetryRecord> statusTelemetryRecordList) {
        // Group records by satellite id
        Map<Integer, List<StatusTelemetryRecord>> satelliteMap = statusTelemetryRecordList.stream()
                .collect(Collectors.groupingBy(StatusTelemetryRecord::getSatelliteId));

        Map<Integer, List<StatusTelemetryRecord>> batteryUnderRedLowSatelliteMap = new HashMap<>();
        Map<Integer, List<StatusTelemetryRecord>> thermostatOverRedHighSatelliteMap = new HashMap<>();

        // Create grouping of each violation condition by satellite id
        for (Map.Entry<Integer, List<StatusTelemetryRecord>> satelliteMapEntry : satelliteMap.entrySet()) {
            List<StatusTelemetryRecord> batteryUnderRedLowList = satelliteMapEntry.getValue().stream()
                    .filter(record -> record.getComponent().equals("BATT"))
                    .filter(record -> (record.getRawValue() < record.getRedLowLimit()))
                    .toList();
            batteryUnderRedLowSatelliteMap.put(satelliteMapEntry.getKey(), batteryUnderRedLowList);

            List<StatusTelemetryRecord> thermostatOverRedHighList = satelliteMapEntry.getValue().stream()
                    .filter(record -> record.getComponent().equals("TSTAT"))
                    .filter(record -> (record.getRawValue() > record.getRedHighLimit()))
                    .toList();
            thermostatOverRedHighSatelliteMap.put(satelliteMapEntry.getKey(), thermostatOverRedHighList);
        }


        List<StatusTelemetryResult> statusTelemetryResultList = new ArrayList<>();
        statusTelemetryResultList.addAll(checkForViolation("RED HIGH", thermostatOverRedHighSatelliteMap));
        statusTelemetryResultList.addAll(checkForViolation("RED LOW", batteryUnderRedLowSatelliteMap));

        ObjectMapper mapper = new ObjectMapper();
        DefaultPrettyPrinter prettyPrinter = new StatusTelemetryResultPrettyPrinter();
        try {
            String jsonStr = mapper.writer(prettyPrinter).writeValueAsString(statusTelemetryResultList);
            System.out.println(jsonStr);
        } catch(JsonProcessingException exception) {
            exception.printStackTrace();
        }
    }

    /**
     * Checks for violation(s) for a specific violation type
     *
     * @param violationType Type of violation type (i.e. RED HIGH)
     * @param violationTypeSatelliteIdMap Group of records by satellite id that meet the specific violation alert
     * @return List of alert(s) to publish for the given violation
     */
    private static List<StatusTelemetryResult> checkForViolation(String violationType, Map<Integer,
            List<StatusTelemetryRecord>> violationTypeSatelliteIdMap) {
        List<StatusTelemetryResult> statusTelemetryResultList = new ArrayList<>();

        // Search each group of records per satellite id to see if the time range violation requirement is met
        for (Map.Entry<Integer, List<StatusTelemetryRecord>> violationTypeSatelliteIdMapEntry
                : violationTypeSatelliteIdMap.entrySet()) {
            List<LocalDateTime> timestampList =
                    new ArrayList<>(violationTypeSatelliteIdMapEntry
                            .getValue()
                            .stream()
                            .map(StatusTelemetryRecord::getTimestamp)
                            .toList());
            Map<LocalDateTime, StatusTelemetryRecord> timestampMap =
                    violationTypeSatelliteIdMapEntry
                            .getValue()
                            .stream()
                            .collect(Collectors.toMap(StatusTelemetryRecord::getTimestamp, record -> record));
            Collections.sort(timestampList);
            int currentTimestampListIndex = 0;
            // Iterate over the timestamps to see if three violation records fall within the 5 minute range requirement
            while (timestampList.size() - currentTimestampListIndex >= 3) {
                double minutesBetweenTimestamps =
                        ChronoUnit.SECONDS.between(timestampList.get(currentTimestampListIndex),
                                timestampList.get(currentTimestampListIndex + 2)) / 60.0;
                if (minutesBetweenTimestamps <= 5.0) {
                    // Store record to publish violation alert
                    StatusTelemetryRecord violationAlertRecord =
                            timestampMap.get(timestampList.get(currentTimestampListIndex));
                    statusTelemetryResultList.add(
                            new StatusTelemetryResult(
                                    violationAlertRecord.getSatelliteId(),
                                    violationType,
                                    violationAlertRecord.getComponent(),
                                    resultDateTimeFormatter.format(violationAlertRecord.getTimestamp())));

                    // Move to the next timestamp after the third timestamp that triggered the violation alert
                    currentTimestampListIndex+=3;
                } else {
                    currentTimestampListIndex++;
                }
            }
        }

        return statusTelemetryResultList;
    }

    /**
     * Class to format the JSON output
     */
    public static class StatusTelemetryResultPrettyPrinter extends DefaultPrettyPrinter {

        @Override
        public DefaultPrettyPrinter createInstance() {
            StatusTelemetryResultPrettyPrinter prettyPrinter = new StatusTelemetryResultPrettyPrinter();
            prettyPrinter.indentArraysWith(DefaultIndenter.SYSTEM_LINEFEED_INSTANCE);
            return prettyPrinter;
        }

        @Override
        public void writeObjectFieldValueSeparator(JsonGenerator jsonGenerator) throws IOException {
            jsonGenerator.writeRaw(": ");
        }
    }
}
