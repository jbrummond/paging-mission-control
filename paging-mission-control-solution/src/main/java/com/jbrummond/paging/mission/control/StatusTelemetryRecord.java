package com.jbrummond.paging.mission.control;

import java.time.LocalDateTime;

/**
 * Status Telemetry Record POJO
 *
 * @author jbrummond
 */
public class StatusTelemetryRecord {

    /**
     * Record timestamp
     */
    private LocalDateTime timestamp;

    /**
     * Satellite id number
     */
    private int satelliteId;

    /**
     * Red high limit
     */
    private int redHighLimit;

    /**
     * Red low limit
     */
    private int redLowLimit;

    /**
     * Actual value of the reading
     */
    private double rawValue;

    /**
     * Type of reading
     */
    private String component;

    public StatusTelemetryRecord() {
    }

    public StatusTelemetryRecord(LocalDateTime timestamp, int satelliteId, int redHighLimit, int redLowLimit,
                                 double rawValue, String component) {
        this.timestamp = timestamp;
        this.satelliteId = satelliteId;
        this.redHighLimit = redHighLimit;
        this.redLowLimit = redLowLimit;
        this.rawValue = rawValue;
        this.component = component;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public int getSatelliteId() {
        return satelliteId;
    }

    public void setSatelliteId(int satelliteId) {
        this.satelliteId = satelliteId;
    }

    public int getRedHighLimit() {
        return redHighLimit;
    }

    public void setRedHighLimit(int redHighLimit) {
        this.redHighLimit = redHighLimit;
    }

    public int getRedLowLimit() {
        return redLowLimit;
    }

    public void setRedLowLimit(int redLowLimit) {
        this.redLowLimit = redLowLimit;
    }

    public double getRawValue() {
        return rawValue;
    }

    public void setRawValue(double rawValue) {
        this.rawValue = rawValue;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }
}
