package com.jbrummond.paging.mission.control;

/**
 * Status Telemetry Result POJO
 *
 * @author jbrummond
 */
public class StatusTelemetryResult {

    /**
     * Satellite id number
     */
    private int satelliteId;

    /**
     * Type of violation
     */
    private String severity;

    /**
     * Component type
     */
    private String component;

    /**
     * Timestamp of the first entry in violation
     */
    private String timestamp;

    public StatusTelemetryResult(int satelliteId, String severity, String component, String timestamp) {
        this.satelliteId = satelliteId;
        this.severity = severity;
        this.component = component;
        this.timestamp = timestamp;
    }

    public int getSatelliteId() {
        return satelliteId;
    }

    public void setSatelliteId(int satelliteId) {
        this.satelliteId = satelliteId;
    }

    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
